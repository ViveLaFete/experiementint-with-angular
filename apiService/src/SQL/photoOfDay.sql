CREATE TABLE IF NOT EXISTS photo_of_day (
  url text NOT NULL,
  explanation text,
  title text NOT NULL,
  date_taken text NOT NULL,
  id INT PRIMARY KEY NOT NULL
);