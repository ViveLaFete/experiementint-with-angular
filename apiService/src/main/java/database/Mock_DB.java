package database;

import dataclasses.nasa_api.Person;
import dataclasses.PhotoOfDay;

import java.util.*;

public class Mock_DB {

    private Set<Person> group_of_objects = new HashSet<>();
    private Set<PhotoOfDay> photoOfDays = new HashSet<>();
    private Map<String, String> urlReference = new HashMap<>();

    public Mock_DB() {
        initUrlReference();
    }

    public boolean addToGroup(Person person) {
        return group_of_objects.add(person);
    }

    public Set<Person> getGroup_of_objects() {
        return group_of_objects;
    }

    public boolean addPhotoToPhotos(PhotoOfDay photo) {
        return photoOfDays.add(photo);
    }

    public Set<PhotoOfDay> getPhotoOfDays() {
        return photoOfDays;
    }

    //https://api.nasa.gov/planetary/earth/imagery?lon=100.75&lat=1.5&date=2014-02-01&cloud_score=True&api_key=DEMO_KEY
    //https://api.nasa.gov/planetary/earth/imagery/?lon=1.5&lat=100.75&date=2016-02-01&cloud_score=true&api_key=DEMO_KEY
    private void initUrlReference() {
        urlReference.put("photoOfDay", "https://api.nasa.gov/planetary/apod?api_key=");
        urlReference.put("earthImage", "https://api.nasa.gov/planetary/earth/imagery/?lon=&lat=&date=&cloud_score=&api_key=");
    }

    public String getUrlToCallFromRef(String ref) {
        return urlReference.get(ref);
    }
}
