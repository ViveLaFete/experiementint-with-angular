package resourcecontrollers;

import com.fasterxml.jackson.databind.ObjectMapper;
import database.Mock_DB;
import dataclasses.AbstractDataClass;
import dataclasses.PhotoOfDay;
import dataclasses.nasa_api.APITranslationService;
import dataclasses.nasa_api.request.EarthImageryInput;
import dataclasses.nasa_api.response.EarthImagery;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
public class NASAController {

    public static final String API_KEY = "HHAXsvv3a4GKwoQjdfAJpRNJDPd8tTmqsODEYSXv";
    private Mock_DB mock_db = new Mock_DB();

    private static final String PHOTO_OF_DAY = "photoOfDay";
    private static final String IMAGE_OF_EARTH = "earthImage";

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "getspaceinfo/photoofday")
    @ResponseBody
    public ResponseEntity<PhotoOfDay> getPhotoOfDay() throws IOException {
        AbstractDataClass response = getAPIResultJSON(mock_db.getUrlToCallFromRef(PHOTO_OF_DAY), PhotoOfDay.class);
        mock_db.addPhotoToPhotos((PhotoOfDay) response);
        if (response.getResponseCode() != 200) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>((PhotoOfDay) response, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "getspaceinfo/imageOfEarth")
    public ResponseEntity<EarthImagery> getImageOfEarth(@RequestBody EarthImageryInput input) throws IOException {
        String[] apiCallToMake = APITranslationService.covertClassVariablesToFlatArray(input);
        String urlToCall = APITranslationService.injectVariablesIntoQueryArgs(apiCallToMake, mock_db.getUrlToCallFromRef(IMAGE_OF_EARTH));
        System.out.println(urlToCall);
        AbstractDataClass response = getAPIResultJSON(urlToCall, EarthImagery.class);
        //DoRepeatYourself
        if (response.getResponseCode() != 200) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>((EarthImagery) response, HttpStatus.OK);
        }
    }

    private AbstractDataClass getAPIResultJSON(String url, Class<? extends AbstractDataClass> clazz) throws IOException {
        AbstractDataClass jsonResponse = null;
        try {
            jsonResponse = clazz.getDeclaredConstructor().newInstance();
            Optional<URL> urlOptional = getURL(url);
            if (urlOptional.isPresent()) {
                jsonResponse = makeSimpleAPIRequestAndSerialise(urlOptional.get(), "GET", PhotoOfDay.class);
            } else {
                Logger.getLogger(NASAController.class.getName()).log(Level.WARNING, PHOTO_OF_DAY + " not configured");
            }
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return jsonResponse;
    }

    private AbstractDataClass makeSimpleAPIRequestAndSerialise(URL url, String requestMethod, Class<? extends AbstractDataClass> deserialiseTo) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(requestMethod);
        String res = getResponseAsString(new BufferedReader(new InputStreamReader(connection.getInputStream())));
        AbstractDataClass dataClass = deserialiseResponse(res, deserialiseTo);
        dataClass.setResponseCode(connection.getResponseCode());
        dataClass.setResponseMessage(connection.getResponseMessage());
        return dataClass;
    }

    private AbstractDataClass deserialiseResponse(String jsonAsString, Class<? extends AbstractDataClass> deserialiseTo) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonAsString, deserialiseTo);
    }

    private List<String> getResponseAsList(BufferedReader responseStream) throws IOException {
        List<String> result = new ArrayList<>();
        String line;
        while ((line = responseStream.readLine()) != null) {
            result.add(line);
        }
        return result;
    }

    private String getResponseAsString(BufferedReader responseStream) throws IOException {
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = responseStream.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }

    private Optional<URL> getURL(String baseUrl) {
        Optional<URL> url = Optional.empty();
        try {
            url = Optional.of(new URL(baseUrl + API_KEY));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }

}
