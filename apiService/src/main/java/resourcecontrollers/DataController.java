package resourcecontrollers;

import database.Mock_DB;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import dataclasses.nasa_api.Person;

import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

@RestController
public class DataController {

    private Mock_DB db = new Mock_DB();

    //http://localhost:8080/spiritofeden/petiteami?name=df
    //cross origin for security reasons
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/spiritofeden/petiteami")
    public Person takeResult(@RequestParam(value="name") String name) {
        double uniqueId = ThreadLocalRandom.current().nextDouble(Double.MAX_VALUE);
        if (name == null || name.isEmpty()) {
            return Person.of("poulet", uniqueId);
        }
        Person hi = Person.of(name, uniqueId);
        if (!db.addToGroup(hi)) {
            //this has somehow got a duplicate id, try again
            takeResult(name);
        }

        System.out.println("Hi has value " + hi.getName());
        return Person.of(reverseName(name), uniqueId);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/spiritofeden/getAllPeople")
    public Set<Person> getAll() {
        return db.getGroup_of_objects();
    }


    private String reverseName(String name) {
        int nameLength = name.length();
        char[] reversedName = new char[nameLength];
        for (int i = nameLength -1; i >= 0; i--) {
            reversedName[nameLength - 1 - i] = name.charAt(i);
        }
        return new String(reversedName);
    }

}