package dataclasses.nasa_api.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EarthImageryInput {

    @JsonProperty("longitude")
    private double longitude;

    @JsonProperty("latitude")
    private double latitude;

    @JsonProperty("date")
    private String date;

    @JsonProperty("cloudCover")
    private boolean cloudCover;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getDate() {
        if (date == null) {
            date = "";
        }
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isCloudCover() {
        return cloudCover;
    }

    public void setCloudCover(boolean cloudCover) {
        this.cloudCover = cloudCover;
    }
}
