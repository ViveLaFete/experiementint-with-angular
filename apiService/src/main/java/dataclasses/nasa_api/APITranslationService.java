package dataclasses.nasa_api;

import dataclasses.nasa_api.request.EarthImageryInput;
import resourcecontrollers.NASAController;

import java.util.ArrayList;
import java.util.List;

public class APITranslationService {

    public static String injectVariablesIntoQueryArgs(String[] input, String url) {
        String[] splitted = url.split("=");

        StringBuilder res = new StringBuilder();
        for (int i = 0; i < splitted.length; i++) {
            res.append(splitted[i]).append("=").append(input[i]);
        }

        return res.toString();
    }

    public static String[] covertClassVariablesToFlatArray(EarthImageryInput input) {
        List<String> ar = new ArrayList<>();
        ar.add(String.valueOf(input.getLongitude()));
        ar.add(String.valueOf(input.getLatitude()));
        ar.add(input.getDate());
        ar.add(String.valueOf(input.isCloudCover()));
        ar.add(NASAController.API_KEY);
        return ar.toArray(new String[0]);
    }

}
