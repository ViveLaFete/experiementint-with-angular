package dataclasses.nasa_api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Resource {

    @JsonProperty("dataset")
    private String dataset;

    @JsonProperty("planet")
    private String planet;

    public String getDataset() {
        return dataset;
    }

    public void setDataset(String dataset) {
        this.dataset = dataset;
    }

    public String getPlanet() {
        return planet;
    }

    public void setPlanet(String planet) {
        this.planet = planet;
    }
}
