package dataclasses.nasa_api;

public class Person {
    
    private final String name;
    private final double id;

    private Person(String input, double id) {
        this.name = input;
        this.id = id;
    }

    public static Person of(String input, double id) {
        return new Person(input, id);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    public double getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Person) {
            Person db = (Person) obj;
            return this.id == db.getId() && this.name.equals(db.name);
        } else {
            return super.equals(obj);
        }
    }
}