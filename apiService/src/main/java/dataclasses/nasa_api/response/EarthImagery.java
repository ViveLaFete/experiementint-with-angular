package dataclasses.nasa_api.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import dataclasses.AbstractDataClass;

public class EarthImagery extends AbstractDataClass {

    @JsonProperty("cloud_score")
    private double cloudScore;

    @JsonProperty("date")
    private String date;

    @JsonProperty("id")
    private String id;

    @JsonProperty("url")
    private String url;

    public double getCloudScore() {
        return cloudScore;
    }

    public void setCloudScore(double cloudScore) {
        this.cloudScore = cloudScore;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
