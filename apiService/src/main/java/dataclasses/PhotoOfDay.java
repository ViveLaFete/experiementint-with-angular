package dataclasses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotoOfDay extends AbstractDataClass {

    @JsonProperty("date")
    private String date;

    @JsonProperty("explanation")
    private String explanation;

    @JsonProperty("url")
    private String url;

    @JsonProperty("title")
    private String title;

    public static PhotoOfDay of(String date, String wording, String url, String title) {
        PhotoOfDay photoOfDay = new PhotoOfDay();
        photoOfDay.setDate(date);
        photoOfDay.setExplanation(wording);
        photoOfDay.setUrl(url);
        photoOfDay.setTitle(title);
        return photoOfDay;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public String getDate() {
        return date;
    }

    public String getExplanation() {
        return explanation;
    }

    public String getImgUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

}
