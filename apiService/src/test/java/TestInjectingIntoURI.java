import org.junit.Assert;
import org.junit.Test;

public class TestInjectingIntoURI {

    @Test
    public void testCanInjectVarsIntoURI() {
        String uri = "https://api.nasa.gov/planetary/earth/imagery?lon=&lat=&date=&cloud_score=&api_key=";
        String longitude = "123";
        String[] splitted = uri.split("=");
        StringBuilder res = new StringBuilder();
        for (String splitPart : splitted) {
            res.append(splitPart).append("=").append(longitude);
        }
        Assert.assertEquals("https://api.nasa.gov/planetary/earth/imagery?lon=123&lat=123&date=123&cloud_score=123&api_key=123",
                res.toString());
    }
}
