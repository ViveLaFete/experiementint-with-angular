export class PhotoOfDay {
    constructor(
        //this also creates a public parameter of this type
        public date: string,
        public explanation: string,
        public url: string,
        public title: string
    ) { }
}