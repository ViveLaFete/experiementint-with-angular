import { Component, OnInit, Input } from '@angular/core';
import { PhotoOfDay } from './photoOfDay';

@Component({
  selector: 'app-photo-of-day',
  templateUrl: './photo-of-day.component.html',
  styleUrls: ['./photo-of-day.component.css']
})
export class PhotoOfDayComponent implements OnInit {
  //master detail component means greater localisation 
  @Input() photo_of_day: PhotoOfDay;

  constructor() { }

  ngOnInit() {
  }

}
