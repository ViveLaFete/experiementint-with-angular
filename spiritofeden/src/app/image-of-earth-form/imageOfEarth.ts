export class ImageOfEarth {
    constructor(
        public date: string,
        public latitude: number,
        public longitude: number,
        public cloudCover: boolean,
    ) {}
}