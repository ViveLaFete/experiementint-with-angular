import { Component, OnInit } from '@angular/core';
import { ImageOfEarth } from './imageOfEarth';
import { DatasenderService } from '../datasender.service';
import { ImageOfEarthResponse } from './imageOfEarthResponse';

@Component({
  selector: 'app-image-of-earth-form',
  templateUrl: './image-of-earth-form.component.html',
  styleUrls: ['./image-of-earth-form.component.css']
})
export class ImageOfEarthFormComponent implements OnInit {

  constructor(private sendData: DatasenderService) { }

  ngOnInit() {
  }

  trueFalse = [true, false];

  model: ImageOfEarth = new ImageOfEarth("", 0, 0, false);
  imageOfEarthData: ImageOfEarthResponse;

  get diagnostic() {
    return JSON.stringify(this.model);
  }

  submit() {
    this.sendData.getImageOfEarth(this.model)
      .subscribe(res => this.imageOfEarthData = res);
  }

}
