export class ImageOfEarthResponse {
    constructor(
        public id: string,
        public date: string,
        public url: string,
        public cloudCover: number,
    ) {
        
    }
}