import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageOfEarthFormComponent } from './image-of-earth-form.component';

describe('ImageOfEarthFormComponent', () => {
  let component: ImageOfEarthFormComponent;
  let fixture: ComponentFixture<ImageOfEarthFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageOfEarthFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageOfEarthFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
