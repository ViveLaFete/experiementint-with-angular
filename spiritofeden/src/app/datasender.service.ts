import { Injectable } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Person } from './person';
import { PhotoOfDay } from './photo-of-day/photoOfDay';
import { ImageOfEarth } from './image-of-earth-form/imageOfEarth';
import { ImageOfEarthResponse } from './image-of-earth-form/imageOfEarthResponse';

@Injectable({
  providedIn: 'root'
})
export class DatasenderService {

  private url: string = "http://localhost:8080/spiritofeden/petiteami?name=";
  private getAllPeopleURL = "http://localhost:8080/spiritofeden/getAllPeople";
  private photoOfDayURL = "http://localhost:8080/getspaceinfo/photoofday";
  private imageOfEarth = "http://localhost:8080/getspaceinfo/imageOfEarth"

  private httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Headers': 'x-requested-with x-uw-act-as'
    })
  };

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('A network error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

  constructor(private http: HttpClient) { }

  sendData(data: string): Observable<any> {
    return this.http.get<string>(this.url + data, this.httpOptions).pipe(catchError(this.handleError));
  }

  //magic
  getAllPeople(): Observable<Person[]> {
    return this.http.get<Person[]>(this.getAllPeopleURL, this.httpOptions).pipe(catchError(this.handleError));
  }

  getPhotoOfDay(): Observable<PhotoOfDay> {
    return this.http.get<PhotoOfDay>(this.photoOfDayURL, this.httpOptions).pipe(catchError(this.handleError));
  }

  getImageOfEarth(imageOfEarthRequest: ImageOfEarth): Observable<ImageOfEarthResponse> {
    return this.http.post<ImageOfEarthResponse>(this.imageOfEarth, imageOfEarthRequest, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

}
