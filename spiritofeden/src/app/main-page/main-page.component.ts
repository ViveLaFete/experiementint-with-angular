import { Component, OnInit } from '@angular/core';
import { DatasenderService } from '../datasender.service';
import { Person } from '../person';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  constructor(private sendData: DatasenderService) { }

  ngOnInit() {
  }
  title="Spirit of Eden";
  bjm = 'src/spiritofeden.jpg';

  data: string = '';
  la_putain: string = '';
  quest_ce_que_heureux = false;
  allPeopleSubitted: Person[] = [];

  getAllPeopleFromAPI(): void {
    this.sendData.getAllPeople().subscribe(response => {
      this.allPeopleSubitted = response;
    });
  }

  //send user input to the server (local spring boot app)
  reverseTheName(): void {
    this.sendData.sendData(this.data).subscribe(response => {
      this.la_putain = response.name;
    }); 
  }

}
