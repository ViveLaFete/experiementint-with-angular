import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideStuffComponent } from './side-stuff.component';

describe('SideStuffComponent', () => {
  let component: SideStuffComponent;
  let fixture: ComponentFixture<SideStuffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideStuffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideStuffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
