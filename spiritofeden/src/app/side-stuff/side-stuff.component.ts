import { Component, OnInit, AfterViewInit } from '@angular/core';
import { LoadInfoService } from '../load-info.service';
import { Info } from '../info';
import { Observable, of, fromEvent } from 'rxjs';

@Component({
  selector: 'app-side-stuff',
  templateUrl: './side-stuff.component.html',
  styleUrls: ['./side-stuff.component.css']
})
export class SideStuffComponent implements AfterViewInit {

  constructor(private loadInfo: LoadInfoService) { }
  information: Info[] = [];
  link = 'https://en.wikipedia.org/wiki/Spirit_of_Eden';

  /**
   * as the objects come in from the async call add them all the the information object
   */
  ngAfterViewInit() {
    this.getJsonInformation();
  }

  getJsonInformation(): void {
    this.loadInfo.getJSON().subscribe(
      info => this.information = info);
  }

  playAudio() {
    const audio = new Audio();
    audio.src = "//upload.wikimedia.org/wikipedia/commons/a/a9/XN_Luscinia_megarhynchos_012.ogg";
    audio.load();
    audio.play();
  }

  //just an example of how observables work
  fromEvent(target, eventName) {
    return new Observable((observer) => {
      const handler = (e) => observer.next(e);

      target.addEventListener(eventName, handler);

      return () => {
        //when complete detach the event handler 
        target.removeEventListener(eventName, handler);
      };
    });
  }
  nameInput = document.getElementById('name') as HTMLInputElement;
  subscription = fromEvent(this.nameInput, 'keydown').subscribe(
    (e: KeyboardEvent) => {
      if (e.keyCode === 27) {
        this.nameInput.value = '';
      }
    });

}
