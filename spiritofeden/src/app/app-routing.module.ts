import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OtherPageComponent } from './other-page/other-page.component';
import { MainPageComponent } from './main-page/main-page.component';
import { ImageOfEarthFormComponent } from './image-of-earth-form/image-of-earth-form.component';

const routes: Routes = [
  {path: '', redirectTo: '/mainpage', pathMatch: 'full'},
  {path: 'mainpage', component: MainPageComponent},
  {path: 'nothing', component : OtherPageComponent},
  { path: 'image-of-earth', component: ImageOfEarthFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


