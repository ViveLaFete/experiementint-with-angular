import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http'; 
import { Info } from './info';


@Injectable({
  providedIn: 'root'
})
export class LoadInfoService {

  constructor(private http: HttpClient) { 
    
  }


  /**
  The results of an observable are not published until a consumer subscribes to it. 
  The subscriber then recieves 'notifications' until the function completes or they unsubscribe 
   */
  public getJSON(): Observable<any> {
    return this.http.get('./src/resources/info.json');
  }

  //this doesn't seem the right way to do this- observables are easy to 
  public getResults(): any[] {
    var results: any[] = [];
    this.getJSON().subscribe(data => results.push(data));
    return results;
  }


}
