export class Info {
    songTitle: string;
    releaseDate: string;
    streams: number;
}