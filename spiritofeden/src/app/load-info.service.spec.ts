import { TestBed } from '@angular/core/testing';

import { LoadInfoService } from './load-info.service';

describe('LoadInfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoadInfoService = TestBed.get(LoadInfoService);
    expect(service).toBeTruthy();
  });
});
