import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 

import { AppComponent } from './app.component';
import { SideStuffComponent } from './side-stuff/side-stuff.component';
import { AppRoutingModule } from './app-routing.module';
import { OtherPageComponent } from './other-page/other-page.component';
import { MainPageComponent } from './main-page/main-page.component';
import { PhotoOfDayComponent } from './photo-of-day/photo-of-day.component';
import { ImageOfEarthFormComponent } from './image-of-earth-form/image-of-earth-form.component';

@NgModule({
  declarations: [
    AppComponent,
    SideStuffComponent,
    OtherPageComponent,
    MainPageComponent,
    PhotoOfDayComponent,
    ImageOfEarthFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
