import { Component, OnInit } from '@angular/core';
import { PhotoOfDay } from '../photo-of-day/photoOfDay';
import { DatasenderService } from '../datasender.service';

@Component({
  selector: 'app-other-page',
  templateUrl: './other-page.component.html',
  styleUrls: ['./other-page.component.css']
})
export class OtherPageComponent implements OnInit {

  photo_of_day: PhotoOfDay;

  constructor(private sendData: DatasenderService) { }

  ngOnInit() {
    //good end state but this will really spam NASAs service
    //this.getImageOfDayFromAPI();
  }

  getImageOfDayFromAPI() {
    this.sendData.getPhotoOfDay().subscribe(
      response => {
        this.photo_of_day = response;
      }
    );
  }

}
