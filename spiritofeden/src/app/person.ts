export class Person {
    constructor(
        //this also creates a public parameter of this type
        public name: string,
        public id: number
    ) {}
}